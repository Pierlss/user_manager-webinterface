<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>SPECS User Manager</title>
	<link href="bootstrap/css/bootstrap.min.css"  type="text/css" rel="stylesheet">
	<style type="text/css">
		body {
			padding-top: 60px;
			padding-bottom: 40px;
		}
	
		.sidebar-nav {
			padding: 9px 0;
		}
	</style>
</head>
<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"> <img alt="Brand"
					src="<%=request.getContextPath()%>/img/specs-logo-32.png"></a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
				</ul>
				<a class="navbar-brand pull-right"
					href="<%=request.getContextPath()%>/index.jsp">Home</a>
			</div>
			<!--/.nav-collapse -->
		</div>
		</nav>
		<div class="jumbotron">
				<div class="container">
		
					${message}
					
					<form method="POST" class="form-horizontal">
		  				<br/><br/>
		  				<div class="form-group">
		    				<label class="control-label col-sm-2" for="username">Username:</label>
		    				<div class="col-sm-3">
		      				<input type="text" name="username" class="form-control" id="username" placeholder="enter username">
		    				</div>
		  				</div>
		  				<div class="form-group">
		    				<label class="control-label col-sm-2" for="pwd">Password:</label>
		    				<div class="col-sm-3"> 
		      				<input type="password" name="password" class="form-control" id="pwd" placeholder="enter password">
		    				</div>
		  				</div>
		  				<div class="form-group">
		  					<label class="control-label col-sm-3" for="sel1">&emsp;Select role:</label><br></br>
		  							<select class="col-sm-offset-2 col-sm-2" name="role" class="form-control" id="sel1">
		    							<option value="user">User</option>
		    							<option value="developer">Developer</option>
		    							<option value="owner">Owner</option>
		   					</select>
						</div>
		  				<div class="form-group"> 
		    				<div class="col-sm-offset-3 col-sm-9" >
		      				<button type="submit" class="btn btn-default">Login</button>
		      				<!--  button type="submit" class="btn btn-default">Signup</button>-->
		   					</div>
		   					
		 				</div>
				</form>
			 </div>
		 </div>
		<jsp:include page="/utils/footer.jsp"/>
</body>
</html>