<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>SPECS User Manager</title>
	<link href="bootstrap/css/bootstrap.min.css"  type="text/css" rel="stylesheet">
	<style type="text/css">
		body {
			padding-top: 60px;
			padding-bottom: 40px;
		}
	
		.sidebar-nav {
			padding: 9px 0;
		}
		
	</style>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"> <img alt="Brand"
					src="<%=request.getContextPath()%>/img/specs-logo-32.png"></a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
				</ul>
				<a class="navbar-brand pull-right"
					href="<%=request.getContextPath()%>/index.jsp">Home</a>
			</div>
			<!--/.nav-collapse -->
		</div>
		</nav>
		
		<div class="jumbotron">
		<div class="container">
			
			<h1>SPECS User Manager</h1>
			<h2>WELCOME</h2>
	
						
	 </div>
 </div>
<jsp:include page="/utils/footer.jsp"/>
</body>
</html>