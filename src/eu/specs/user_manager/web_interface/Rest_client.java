package eu.specs.user_manager.web_interface;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.ResponseEntity;

public class Rest_client {
	
	public static String processLoginReq(@RequestBody String u){
		// Sends request to OpenLDAP Framework and receives response to it:
		// if response is "OK", forwards request to XACML Framework, and 
		// wait the response (page)
		final String url1="http://localhost:8080/OpenLDAPFramework/login.jsp";
		final String url2="http://localhost:8080/XACMLFramework/login.jsp";
		
		//Sending request to OpenLDAP Framework
		//System.out.println("[Web Interface] Sending Request of: "+u);
		RestTemplate rt= new RestTemplate();
		String res1 = rt.postForObject(url1, u, String.class);
		//URI res12 = rt.postForLocation(url, u, String.class);
		//ResponseEntity<String> res13 = rt.postForEntity(url, u, String.class);
		System.out.println("[Web Interface] Signup response: "+res1);
		//System.out.println("[Web Interface] res12: "+res12);
		//System.out.println("[WebInterface] res13: "+res13);
		
		if(res1.equals("OK")==true){
			//Send request to OpenLDAP Framework and handle the response
			RestTemplate rt2= new RestTemplate();
			String res2 = rt2.postForObject(url2, u, String.class);
			if(res2.equals("errorPage")==false)
				System.out.println("[Web Interface] Valid Login ");
			else System.err.println("[Web Interface] Invalid Login ");
			return res2;
		}else if(res1.equals("NOT OK")==true){
				System.err.println("[Web Interface] Invalid Login ");
				return "errorPage";
			  }else{
				System.err.println("[Web Interface] Invalid Login ");
				return "errorPage";
		}
	}

	
	public static String processSignupReq(@RequestBody String u) {
		//Sends Signup Request to OpenLDAP Framework and receives response to it:
		// If response is "OK", a new account is created.
		final String url="http://localhost:8080/OpenLDAPFramework/signup.jsp";
		RestTemplate rt= new RestTemplate();
		String res = rt.postForObject(url, u, String.class);
		System.out.println("[Web Interface] Signup response: "+res);
		
		if(res.equals("OK")==true){
			return "successSignup";
		}else{
			return "errorSignup";
		}
		
	}
}