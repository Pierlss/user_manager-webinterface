package eu.specs.user_manager.web_interface;

public class User {
	
	private String username;
	private String password;
	private String role;
	
	public User(){
		
	}

	public User(String u,String p,String r){
		this.username=u;
		this.password=p;
		this.role=r;
	}
	

	public void setUsername(String u){
		this.username=u;
	}
	
	public void setPassword(String p){
		this.password=p;
	}
	
	public void setRole(String r){
		this.role=r;
	}
	
	public String getUsername(){
		return this.username;
	}
	
	public String getPassword(){
		return this.password;
	}
		
	public String getRole(){
		return this.role;
	}
	
}
