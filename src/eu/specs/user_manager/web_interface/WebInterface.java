package eu.specs.user_manager.web_interface;

import org.json.simple.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class WebInterface {
	
	// Login Servlet: GET Login Form
	@RequestMapping(value="/login",method=RequestMethod.GET)
	public ModelAndView login() {
 
		String message = "<h1>SPECS User Manager<h1>" +
						 "<h2>LOGIN</h2>" +
						 "<h3>Please insert SPECS credentials and role to access."+
						 "<br/><br/></h3>";
		return new ModelAndView("login", "message", message);
	}
	
	
	// Login Servlet: POST the triple
	@RequestMapping(value="/login",method=RequestMethod.POST)
	//@ResponseBody 
	public String loginReq(@ModelAttribute("SpringWeb")User user, 
			   ModelMap model){
		
		model.addAttribute("username", user.getUsername());
	    model.addAttribute("password", user.getPassword());
	    model.addAttribute("role", user.getRole());
	    
	    String u="{"+user.getUsername()+","+user.getPassword()+","+user.getRole()+"}";
	    System.out.println("[Web Interface] Login Request by: "+user.getUsername()+", "+user.getPassword()+", "+ user.getRole());
	    String message ="<h1>SPECS User Manager<h1> Login Request: "+u;
	   
	    //Send received data to the function that sends it to out
	    return sendLoginReq(u);
	}
	
	//Send the triple
	public String sendLoginReq(String user){
		//String u="{"+user.getUsername()+","+user.getPassword()+","+user.getRole()+"}";
		String dest=Rest_client.processLoginReq(user);//Redirect String Page
		
		return dest;
	}
	
	// Signup Servlet: GET Signup Form
	@RequestMapping(value="/signup",method=RequestMethod.GET)
	public ModelAndView signup() {
 
		String message = "<h1>SPECS User Manager<h1>" +
						 "<h2>SIGNUP</h2>" +
						 "<h3>Please insert SPECS credentials and select role into SPECS APPS."+
						 "<br/><br/></h3>";
		return new ModelAndView("signup", "message", message);
	}

	// Signup Servlet: POST the triple
	@RequestMapping(value="/signup",method=RequestMethod.POST)
	public String signupReq(@ModelAttribute("SpringWeb")User user, 
			   ModelMap model){
		
		model.addAttribute("username", user.getUsername());
	    model.addAttribute("password", user.getPassword());
	    model.addAttribute("role", user.getRole());
	    
	    String u="{"+user.getUsername()+","+user.getPassword()+","+user.getRole()+"}";
	    System.out.println("[Web Interface] Signup Request by: "+user.getUsername()+", "+user.getPassword()+", "+ user.getRole());
	    String message ="<h1>SPECS User Manager<h1> Login Request: "+u;
	    
	  //Send received data to the function that sends it to out
	    return sendSignupReq(u);
	}
	
	//Send the triple (in Signup)
	public String sendSignupReq(String user){
			
			//String u="{"+user.getUsername()+","+user.getPassword()+","+user.getRole()+"}";
			String dest=Rest_client.processSignupReq(user);//"<Stringa dove deve fare redirect>"
			
			return dest;
		}
}
